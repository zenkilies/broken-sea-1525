package main

import "gitlab.com/zenkilies/broken-sea-1525/src/server"

func main() {
	e := server.NewServer()
	e.Logger.Fatal(e.Start(":1323"))
}
